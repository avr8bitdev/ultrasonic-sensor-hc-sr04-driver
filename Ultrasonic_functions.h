/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_ULTRASONIC_DRIVER_ULTRASONIC_FUNCTIONS_H_
#define HAL_DRIVERS_ULTRASONIC_DRIVER_ULTRASONIC_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

typedef struct
{
    DIO_port_t portTrig;
    u8 pinTrig;

    DIO_port_t portEcho;
    u8 pinEcho;

} Ultrasonic_obj_t;

typedef const Ultrasonic_obj_t* Ultrasonic_cptr_t;

void Ultrasonic_vidInit(Ultrasonic_cptr_t structPtrUltrasonicCpy);

u16 Ultrasonic_u16GetTime(Ultrasonic_cptr_t structPtrUltrasonicCpy);
f32 Ultrasonic_f32GetDistance(Ultrasonic_cptr_t structPtrUltrasonicCpy);
void Ultrasonic_vidGetTimeDistance(Ultrasonic_cptr_t structPtrUltrasonicCpy, u16* u16PtrTimeCpy, f32* f32PtrDistance);

void Ultrasonic_vidMedianFilter(u16* u16PtrDataCpy, const u16 u16LenCpy);


#endif /* HAL_DRIVERS_ULTRASONIC_DRIVER_ULTRASONIC_FUNCTIONS_H_ */
