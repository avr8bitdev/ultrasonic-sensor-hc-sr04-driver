/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Ultrasonic_functions.h"
#include "../../MCAL_drivers/Timer_driver/Timer_functions.h"
#include <util/delay.h>


#define ULTRASONIC_TIMER_TIMEOUT_US 16 // Timer timeout
#define ULTRASONIC_TIMER_PRESCALER  8  // Timer prescaler div factor
#define ULTRASONIC_TIMER_PERIOD (ULTRASONIC_TIMER_PRESCALER / (F_CPU / 1e6)) // period of 1 tick

#define ULTRASONIC_3CM_TIME_US   177   // time [us] of 3cm
#define ULTRASONIC_400CM_TIME_US 23529 // time [us] of 400cm

// --- internal --- //
static void Ultrasonic_vidSelectionSort_asc(u16* u16PtrDataCpy, const u16 u16LenCpy)
{
    for (u16 current_idx = 0; current_idx < u16LenCpy; current_idx++) // foreach element is src
    {
        u16 idx_of_least = current_idx; // assume smallest is current element
        for (u16 next_idx = current_idx + 1; next_idx < u16LenCpy; next_idx++) // starting from next element after current one
        {
            if (u16PtrDataCpy[next_idx] < u16PtrDataCpy[idx_of_least])
                idx_of_least = next_idx;
        }

        if (idx_of_least != current_idx) // if a smaller no. is found, then swap
        {
            u16 tmp_ = u16PtrDataCpy[current_idx];
            u16PtrDataCpy[current_idx] = u16PtrDataCpy[idx_of_least];
            u16PtrDataCpy[idx_of_least] = tmp_;
        }
    }
}
// ---------------- //

void Ultrasonic_vidInit(Ultrasonic_cptr_t structPtrUltrasonicCpy)
{
    DIO_vidSet_pinDirection(structPtrUltrasonicCpy->portTrig, structPtrUltrasonicCpy->pinTrig, OUTPUT);
    DIO_vidSet_pinValue(structPtrUltrasonicCpy->portTrig, structPtrUltrasonicCpy->pinTrig, OFF);

    DIO_vidSet_pinDirection(structPtrUltrasonicCpy->portEcho, structPtrUltrasonicCpy->pinEcho, INPUT);

    // this is not need because the echo pin will always impose a value (0 or 1)
    //DIO_vidActivate_pinPullUp(structPtrUltrasonicCpy->portEcho, structPtrUltrasonicCpy->pinEcho);
}

u16 Ultrasonic_u16GetTime(Ultrasonic_cptr_t structPtrUltrasonicCpy)
{
    Timer_vidInit(Timer0_prescaler_off, Timer_mode_CTC, 0); // Timer mode: CTC
    Timer_vidSetCmpValue(ULTRASONIC_TIMER_TIMEOUT_US / ULTRASONIC_TIMER_PERIOD, 0); // 16 uS (gives resolution 0.272 cm)

    // holds the amount of wasted time during high pulse of echo [us]
    u16 time_micro_sec = 0;

    // send trigger pulse (at least 10 uS)
    DIO_vidSet_pinValue(structPtrUltrasonicCpy->portTrig, structPtrUltrasonicCpy->pinTrig, ON);
    _delay_us(10);
    DIO_vidSet_pinValue(structPtrUltrasonicCpy->portTrig, structPtrUltrasonicCpy->pinTrig, OFF);

    // wait until pin is high
    while ( !DIO_u8Get_pinValue(structPtrUltrasonicCpy->portEcho, structPtrUltrasonicCpy->pinEcho) )
    {}

    // turn on timer
    Timer_vidSetClock(Timer0_prescaler_div_8, 0);

    // wait until pin is low
    do
    {
        if (Timer_u8isCmpMatch(0))
        {
            Timer_vidResetCmpMatchFlag(0);
            time_micro_sec += ULTRASONIC_TIMER_TIMEOUT_US;
        }

    } while ( DIO_u8Get_pinValue(structPtrUltrasonicCpy->portEcho, structPtrUltrasonicCpy->pinEcho) );

    // turn off timer
    Timer_vidSetClock(Timer0_prescaler_off, 0);

    // wait over 60ms as a measurement cycle, to avoid sending trigger signal during next echo (as per datasheet)
    //_delay_ms(60);

    // value in cm
    if ( (time_micro_sec < ULTRASONIC_3CM_TIME_US) || (time_micro_sec > ULTRASONIC_400CM_TIME_US) )
        return 0;
    else
        return time_micro_sec / 2;
}

f32 Ultrasonic_f32GetDistance(Ultrasonic_cptr_t structPtrUltrasonicCpy)
{
	return Ultrasonic_u16GetTime(structPtrUltrasonicCpy) / 29.4118; // * (340) * (100 / (1000^2))
}

void Ultrasonic_vidGetTimeDistance(Ultrasonic_cptr_t structPtrUltrasonicCpy, u16* u16PtrTimeCpy, f32* f32PtrDistance)
{
	*u16PtrTimeCpy = Ultrasonic_u16GetTime(structPtrUltrasonicCpy);
	*f32PtrDistance = *u16PtrTimeCpy / 29.4118; // * (340) * (100 / (1000^2))
}

void Ultrasonic_vidMedianFilter(u16* u16PtrDataCpy, const u16 u16LenCpy)
{
    u16 res[16];

    // --- do 1st iteration manual --- //
    // prepare the 3-point window
    u16 window[3] = { u16PtrDataCpy[0], u16PtrDataCpy[0], u16PtrDataCpy[1] };
    // sort window asc
    Ultrasonic_vidSelectionSort_asc(window, 3);
    // save result
    res[0] = window[1];
    // ------------------------------- //

    u16 i = 0;
    for (i = 0; i < u16LenCpy - 2; i++)
    {
        // prepare the 3-point window
        window[0] = u16PtrDataCpy[i];
        window[1] = u16PtrDataCpy[i + 1];
        window[2] = u16PtrDataCpy[i + 2];
        // sort window asc
        Ultrasonic_vidSelectionSort_asc(window, 3);
        // save result
        res[i + 1] = window[1];
    }

    // --- do last iteration manual --- //
    // prepare the 3-point window
    window[0] = u16PtrDataCpy[u16LenCpy - 2];
    window[1] = u16PtrDataCpy[u16LenCpy - 1];
    window[2] = u16PtrDataCpy[u16LenCpy - 1];
    // sort window asc
    Ultrasonic_vidSelectionSort_asc(window, 3);
    // save result
    res[i + 1] = window[1];
    // -------------------------------- //

    // copy data back
    for (i = 0; i < u16LenCpy; i++)
        u16PtrDataCpy[i] = res[i];
}

